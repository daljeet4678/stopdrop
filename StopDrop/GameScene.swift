//
//  GameScene.swift
//  StopDrop
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
  
    let mario = SKSpriteNode(imageNamed: "mario")
 
    
    var timeOfLastUpdate:TimeInterval = 0
    var dt: TimeInterval = 0
    
    override func didMove(to view: SKView) {
         let m1 = SKAction.moveBy(x: -500, y: 0, duration: 2)
      let m2 = SKAction.moveBy(x: 500, y: 0, duration: 2)
    let sequence = SKAction.sequence([m1,m2])
        self.mario.run(SKAction.repeatForever(sequence))
    }
    func spawnmario() {
        let mario = SKSpriteNode(imageNamed:"mario")
        
        // put sand at a random (x,y) position
        let x = self.size.width/2
        let y = self.size.height - 100
        mario.position.x = x
        mario.position.y = y
        
        // add physics
        mario.physicsBody = SKPhysicsBody(circleOfRadius: mario.size.width / 2)
        self.mario.physicsBody?.affectedByGravity = true
        
        addChild(mario)
    }
    func makeGround(xPosition:CGFloat, yPosition:CGFloat) {
        
      
        let ground = SKSpriteNode(imageNamed: "platform")
        ground.position.x = xPosition;
        ground.position.y = yPosition;
        addChild(ground)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
             let touch = touches.first!
            let mousePosition = touch.location(in:self)
            
            
           // self.makeGround(xPosition: CGFloat, yPosition: CGFloat)
       self.makeGround(xPosition:mousePosition.x,
            yPosition:mousePosition.y)
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // make new sand every 10ms
        self.dt = currentTime - timeOfLastUpdate
        if (self.dt >= 2) {
            timeOfLastUpdate = currentTime
            self.spawnmario()
        }
        
        
    }
}
